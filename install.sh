#!/bin/bash
echo Installing dotnet-warp...
dotnet tool install --global dotnet-warp
export PATH="$PATH:/home/$USER/.dotnet/tools"
echo Creating self containing application...
cd src/App
dotnet-warp -o FreeRdpClient
cd ../../
echo Creating shortcut on /usr/share/application...
sudo cp -f install_files/FreeRdpClient.desktop /usr/share/applications/FreeRdpClient.desktop
echo Copying icon and application to /usr/bin/FreeRdpClient...
sudo mkdir -p /usr/bin/FreeRdpClient/
sudo cp -f install_files/Icon.ico /usr/bin/FreeRdpClient/Icon.ico
sudo cp -f src/App/FreeRdpClient /usr/bin/FreeRdpClient/App
echo Done.