# free-rdp-client

This project was created to easily create an RDP connection with multiple monitors.
This is a private project that may not be developed further. I will not provide any support.

## Requirements

* Git
* .NET Core 3.1 SDK
* xfreerdp
* Linux

## GTK# application

### Installation

Clone the application.

~~~
git clone https://gitlab.com/pknaepel/freerdp-cli-helper.git
~~~

Go to main folder.

~~~
cd freerdp-cli-helper/
~~~

Execute installation script

~~~
./install.sh
~~~

Run the app.

## Console application

### CLI Options

|Short Param|Long Param|Required|Description|
|---|---|---|---|
|c|computer|true|Name of computer to connect with.|
|u|username|true|Username to connect with.|
|d|domain|false|Domain of user to connect with.|
|p|password|true|Password of user to connect with.|
|g|gateway|false|Gateway to connect over.|
|s|dual|false|Switch to activate dual screen or not.|

### Example

~~~
dotnet Console.dll -c COMPUTER_NAME -u USER_NAME -p USER_PASSWORD -s true
~~~
