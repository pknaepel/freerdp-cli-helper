using System;
using CommandLine;

namespace Console
{
    public class CommandLineOptions
    {
        [Option('c', "computer", Required = true, HelpText = "Computer name to connect to.")]
        public string Computer { get; set; }

        [Option('u', "username", Required = true, HelpText = "Username to connect with.")]
        public string Username { get; set; }

        [Option('d', "domain", Required = false, HelpText = "Domain of user to connect with.")]
        public string Domain { get; set; }

        [Option('p', "password", Required = true, HelpText = "Password of user to connect with.")]
        public string Password { get; set; }

        [Option('g', "gateway", Required = false, HelpText = "Gateway server")]
        public string GatewayServer { get; set; }

        [Option('s', "dual", HelpText = "Switch to activate dual screen or not.")]
        public bool DualScreen { get; set; }
    }
}