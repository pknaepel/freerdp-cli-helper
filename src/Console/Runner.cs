using Microsoft.Extensions.Logging;
using Services.Interfaces;
using Models;

namespace Console
{
    public class Runner
    {
        private readonly ILogger<Runner> _logger;
        private readonly IFreeRDPService _freeRDPService;

        public Runner(ILogger<Runner> logger, IFreeRDPService freeRDPService)
        {
            _logger = logger;
            _freeRDPService = freeRDPService;
        }

        public void DoAction(CommandLineOptions opts)
        {
            _freeRDPService.Connect(new FreeRDPOptions
            {
                Computer = opts.Computer,
                Domain = opts.Domain,
                Username = opts.Username,
                Password = opts.Password,
                GatewayServer = opts.GatewayServer,
                DualScreen = opts.DualScreen
            });
        }
    }
}