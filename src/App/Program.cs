using System;
using Gtk;
using Helper;
using Services;

namespace App
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init();

            var app = new Application("org.App.App", GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            var commandBuilderHelper = new CommandBuilderHelper();
            var consoleHelper = new ConsoleHelper();
            var freeRdpService = new FreeRDPService(commandBuilderHelper, consoleHelper);

            var win = new MainWindow(freeRdpService);
            app.AddWindow(win);

            win.Show();
            Application.Run();
        }
    }
}
