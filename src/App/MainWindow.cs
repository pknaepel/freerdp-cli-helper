using System;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;
using Services.Interfaces;
using System.Threading.Tasks;

namespace App
{
    class MainWindow : Window
    {
        [UI] private Button btn_connect = null;
        [UI] private Entry txt_compuer = null;
        [UI] private Entry txt_domain = null;
        [UI] private Entry txt_username = null;
        [UI] private Entry txt_password = null;
        [UI] private Entry txt_gateway = null;
        [UI] private CheckButton tgl_dualscreen = null;
        private readonly IFreeRDPService _freeRdpService;

        public MainWindow(IFreeRDPService freeRdpService) : this(new Builder("MainWindow.glade"), freeRdpService) { }

        private MainWindow(Builder builder, IFreeRDPService freeRdpService) : base(builder.GetObject("MainWindow").Handle)
        {
            _freeRdpService = freeRdpService;

            builder.Autoconnect(this);

            var freeRdpOptions = _freeRdpService.Load();
            txt_compuer.Text = freeRdpOptions.Computer;
            txt_domain.Text = freeRdpOptions.Domain;
            txt_username.Text = freeRdpOptions.Username;
            txt_password.Text = freeRdpOptions.Password;
            txt_gateway.Text = freeRdpOptions.GatewayServer;
            tgl_dualscreen.Active = freeRdpOptions.DualScreen;

            DeleteEvent += Window_DeleteEvent;
            btn_connect.Clicked += BtnConnectClicked;
        }

        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }

        private async void BtnConnectClicked(object sender, EventArgs a)
        {
            var freeRdpOptions = new Models.FreeRDPOptions
            {
                Computer = txt_compuer.Text,
                Domain = txt_domain.Text,
                Username = txt_username.Text,
                Password = txt_password.Text,
                GatewayServer = txt_gateway.Text,
                DualScreen = tgl_dualscreen.Active
            };

            _freeRdpService.Save(freeRdpOptions);
            await Task.Run(() => _freeRdpService.Connect(freeRdpOptions));
        }
    }
}
