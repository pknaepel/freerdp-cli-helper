using System;

namespace Models
{
    public class FreeRDPOptions
    {
        public string Computer { get; set; }
        public string Domain { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string GatewayServer { get; set; }
        public bool DualScreen { get; set; }
    }
}
