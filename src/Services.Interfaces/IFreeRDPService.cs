﻿using System;
using Models;

namespace Services.Interfaces
{
    public interface IFreeRDPService
    {
        void Connect(FreeRDPOptions options);
        void Save(FreeRDPOptions options);
        FreeRDPOptions Load();
    }
}
