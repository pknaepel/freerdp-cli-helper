using System;
using Models;

namespace Helper.Interfaces
{
    public interface ICommandBuilderHelper
    {
        string Build(FreeRDPOptions options);
    }
}
