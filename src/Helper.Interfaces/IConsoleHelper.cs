using System;

namespace Helper.Interfaces
{
    public interface IConsoleHelper
    {
        string ExecuteCmd(string cmd);
    }
}
