using System;
using Helper.Interfaces;
using Models;
using System.Linq;

namespace Helper
{
    public class CommandBuilderHelper : ICommandBuilderHelper
    {
        public string Build(FreeRDPOptions options)
        {
            var cmdOptions = new []
            {
                $"/v:{options.Computer}",
                $"/u:{options.Username}",
                $"/p:\"{options.Password}\"",
                "/audio-mode:0",
                "/microphone",
                "/cert-ignore"
            };

            var cmdToggles = new []
            {
                "+clipboard",
                "+fonts"
            };

            if(!string.IsNullOrWhiteSpace(options.GatewayServer))
            {
                cmdOptions = cmdOptions.Concat(new [] { $"/g:{options.GatewayServer}" }).ToArray();
            }

            if(!string.IsNullOrWhiteSpace(options.Domain))
            {
                cmdOptions = cmdOptions.Concat(new [] { $"/d:{options.Domain}" }).ToArray();
            }

            if(options.DualScreen)
            {
                cmdOptions = cmdOptions.Concat(new [] { "/multimon" }).ToArray();
            }
            else
            {
                cmdOptions = cmdOptions.Concat(new [] { "/dynamic-resolution" }).ToArray();
                cmdToggles = cmdToggles.Concat(new [] { "/toggle-fullscreen" }).ToArray();
            }

            return $"xfreerdp {string.Join(" ", cmdOptions)} {string.Join(" ", cmdToggles)}";
        }
    }
}
