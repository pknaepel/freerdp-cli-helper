using System;
using Helper.Interfaces;
using System.Diagnostics;

namespace Helper
{
    public class ConsoleHelper : IConsoleHelper
    {
        public string ExecuteCmd(string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            return process.StandardOutput.ReadToEnd();
        }
    }
}
