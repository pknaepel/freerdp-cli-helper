﻿using System;
using Models;
using Helper.Interfaces;
using Services.Interfaces;
using Newtonsoft.Json;
using System.IO;

namespace Services
{
    public class FreeRDPService : IFreeRDPService
    {
        private readonly ICommandBuilderHelper _commandBuilderHelper;
        private readonly IConsoleHelper _consoleHelper;

        public FreeRDPService(ICommandBuilderHelper commandBuilderHelper, IConsoleHelper consoleHelper)
        {
            _commandBuilderHelper = commandBuilderHelper;
            _consoleHelper = consoleHelper;
        }

        public void Connect(FreeRDPOptions options)
        {
            var logFile = GetLogFilePath();
            var cmd = _commandBuilderHelper.Build(options);
            File.AppendAllText(logFile, $"{DateTime.Now.ToString()}|FreeRdpService|Executing command '{cmd}' ...\n");
            var res =_consoleHelper.ExecuteCmd(cmd);
            File.AppendAllText(logFile, $"{DateTime.Now.ToString()}|FreeRdpService|Shell result '{res}'\n");
        }

        public FreeRDPOptions Load()
        {
            var filePath = GetSettingsFilePath();

            if (!File.Exists(filePath))
            {
                return new FreeRDPOptions();
            }

            var fileContents = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<FreeRDPOptions>(fileContents);

        }

        public void Save(FreeRDPOptions options)
        {
            var json = JsonConvert.SerializeObject(options);
            var filePath = GetSettingsFilePath();
            File.WriteAllText(filePath, json);
        }

        string GetSettingsFilePath()
        {
            var userPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            var folder = $"{userPath}/.free-rdp-client";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            return $"{folder}/rdp_options.json";
        }

        string GetLogFilePath()
        {
            var userPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            var folder = $"{userPath}/.free-rdp-client";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            return $"{folder}/logs_{DateTime.Now.ToString("yyyy-MM-dd")}.txt";
        }
    }
}
